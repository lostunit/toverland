import React, {Component} from 'react';
import {Platform, FlatList, ActivityIndicator, Text, Image, View, TouchableNativeFeedback, Animated, Easing} from 'react-native';
import {ListItem, Button, Header} from 'react-native-elements';

import Style from '../style/style';

const styles = Style.styles;

export default class HomeScreen extends React.Component {

    static navigationOptions = {
        headerStyle: {
            height: 0,
            overflow: 'hidden',
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            activities: null,
            currentActivity: null,
            slideAnimValue: new Animated.Value(100)
        }
    }

    componentDidMount() {
        slideAnim = Animated.timing(
            this.state.slideAnimValue,
            {
                toValue: 0,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }
        )

        return fetch('https://51north.nl/projects/toverland/activities.json')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    activities: responseJson.data,
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    activityItem = ({item}) => {
        return (
            <ListItem
                title={item.name}
                subtitle={item.type}
                avatar={{ uri: item.image }}
                onPress={() => this.setState({ currentActivity: item }) }
            />
        );
    }

    showAloneLength(activity) {
        if (activity.length_alone) {
            return (
                <Text>Min. lengte zelfstandig: {activity.length_alone}</Text>
            );
        }
        return null;
    }

    showGuidedLength(activity) {
        if (activity.length_guided) {
            return (
                <Text>Onder begeleiding: {activity.length_guided}</Text>
            );
        }
        return null;
    }

    render() {

        if (!this.state.activities) {
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator/>
                </View>
            )
        }

        const list = (
            <FlatList
                data={this.state.activities}
                renderItem={this.activityItem}
                keyExtractor={({id}, index) => id}
            />
        );

        let activityBar = null;
        if (this.state.currentActivity) {
            activityBar = (
                <TouchableNativeFeedback
                    onPress={() => this.props.navigation.navigate('Detail', {
                        item: this.state.currentActivity
                    })}
                >
                    <Animated.View style={{
                        height: 100,
                        backgroundColor: '#fff',
                        borderTopWidth: 1,
                        borderTopColor: '#ccc',
                        flexDirection: 'row',
                        translateY: this.state.slideAnimValue
                    }}>
                        <Image source={{uri: this.state.currentActivity.image}} style={styles.activityImage} />
                        <View style={styles.activityText}>
                            <Text style={styles.activityName}>{this.state.currentActivity.name}</Text>
                            {this.showAloneLength(this.state.currentActivity)}
                            {this.showGuidedLength(this.state.currentActivity)}
                        </View>
                    </Animated.View>
                </TouchableNativeFeedback>
            );
            slideAnim.start();
        }

        return (
            <View style={styles.container}>
                {list}
                {activityBar}
            </View>
        );
    }
}
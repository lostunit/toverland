import React, {Component} from 'react';
import {Platform, Text, ImageBackground, View} from 'react-native';

import Style from '../style/style';

const styles = Style.styles;

export default class DetailScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const item = navigation.getParam('item');
        return {
            title: item.type + ': ' + item.name
        }
    };

    render() {
        const navigation = this.props.navigation;
        const item = navigation.getParam('item');
        return (
            <View style={styles.container}>
                <ImageBackground source={{uri: item.image}} style={styles.headerImage}>
                    <Text style={styles.detailTitle}>{item.name}</Text>
                    <Text style={styles.detailSubtitle}>{item.title}</Text>
                </ImageBackground>
                <Text style={styles.detailText}>{item.description}</Text>
            </View>
        );
    }
}
import {StyleSheet} from 'react-native';

export default {
    styles: StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#f6f6f6',
        },
        loadingContainer: {
            flex: 1,
            justifyContent: 'center',
            backgroundColor: '#f6f6f6',
        },

        activityBar: {
            height: 100,
            backgroundColor: '#fff',
            borderTopWidth: 1,
            borderTopColor: '#ccc',
            flexDirection: 'row',
        },
        activityImage: {
            width: 100,
        },
        activityText: {
            flexDirection: 'column',
            justifyContent: 'center',
            paddingLeft: 15,
        },
        activityName: {
            fontWeight: 'bold',
        },

        headerImage: {
            height: 200,
            justifyContent: 'center',
        },
        detailTitle: {
            color: '#fff',
            fontWeight: 'bold',
            fontSize: 32,
            textAlign: 'center',
            textShadowOffset: {width: 2, height: 2},
            textShadowRadius: 5,
        },
        detailSubtitle: {
            color: '#fff',
            fontSize: 18,
            textAlign: 'center',
            textShadowOffset: {width: 2, height: 2},
            textShadowRadius: 10,
        },
        detailText: {
            padding: 15,
            margin: 15,
            backgroundColor: '#fff',
        },
    }),
}